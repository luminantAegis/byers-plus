import requests
import json
import time
import datetime
import traceback
import os
import shutil
from PIL import Image

def seconds_to_fancy_time(seconds):
    seconds = int(seconds)
    minutes, seconds = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)
    periods = [('hours', hours), ('minutes', minutes), ('seconds', seconds)]
    time_string = ', '.join('{} {}'.format(value, name)
                            for name, value in periods
                            if value)
    return '-{0:0=2d}:{1:0=2d}'.format(minutes, seconds)

BYERS_SETTINGS_PATH = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'byersPlus.json')
BYERS_SETTINGS = json.load(open(BYERS_SETTINGS_PATH, 'r', encoding='utf-8-sig'))
BEEFWEB_PORT = BYERS_SETTINGS['beefweb_port']
BEEFWEB_COLUMNS = {
    'album': {'out': 'album', 'transform': None},
    'artist': {'out': 'artist', 'transform': None},
    'title': {'out': 'title', 'transform': None},
    'playback_time_remaining_seconds': {'out': 'remaining', 'transform': seconds_to_fancy_time}
}
LAST_INDEX = None
LAST_VALUES = {}
QUEUE = None

def poll():
    columns = ','.join(["%25{0}%25".format(k) for k, v in BEEFWEB_COLUMNS.items()])
    resolver = list(BEEFWEB_COLUMNS.values())
    url = 'http://127.0.0.1:%d/api/player?columns=%s' % (BEEFWEB_PORT, columns)
    state_obj = requests.get(url).json()
    current = {}
    for ind, column in enumerate(state_obj['player']['activeItem']['columns']):
        output = None
        resolv = resolver[ind]
        if resolv['transform'] is None:
            output = column
        else:
            output = resolv['transform'](column)
        current[resolv['out']] = output
    current['playlist'] = state_obj['player']['activeItem']['playlistId']
    current['index'] = state_obj['player']['activeItem']['index']
    try:
        img_obj = requests.get('http://127.0.0.1:%d/api/artwork/%s/%d' % (BEEFWEB_PORT, state_obj['player']['activeItem']['playlistId'], state_obj['player']['activeItem']['index'])).content
        current['artwork'] = img_obj
    except:
        print('Error while fetching artwork')
    queue = requests.get('http://127.0.0.1:%d/api/queue' % BEEFWEB_PORT).json()['queue']
    queue_list = []
    for item in queue:
        song_res = requests.get('http://127.0.0.1:%d/api/playlists/%s/items/%d:1?columns=%%25title%%25' % (BEEFWEB_PORT, item['playlistId'], item['itemIndex'])).json()
        queue_list.append(song_res['playlistItems']['items'][0]['columns'][0])
    update = {'current': current, 'queue': queue_list}
    return update

def resize():
    try:
        img = Image.open('artwork.png')
        img_resized = img.resize((260, 260))
        img_resized.save('artwork_resized.png')
    except:
        img = Image.new('RGB', (260, 260), color=0)
        img.save('artwork_resized.png')

def update_artwork(poll_result, k, v):
    print(f"Updating artwork to playlist {poll_result['current']['playlist']} and index {poll_result['current']['index']}...")
    try:
        with open(f'{k}.png', 'wb') as f:
            f.write(v)
        resize()
        global LAST_INDEX
        LAST_INDEX = f"{poll_result['current']['playlist']}.{poll_result['current']['index']}"
    except Exception as ex:
        traceback.print_exc()

def update_text_file(poll_result, k, v):
    print(f"Updating {k}.txt with value: {str(v)}")
    try:
        try:
            shutil.copyfile(f'{k}.txt', f'{k}_last.txt')
        except:
            traceback.print_exc()
        with open(f'{k}.txt', 'w', encoding='utf-8-sig') as f:
            f.write(str(v))
        LAST_VALUES[k] = v
    except Exception as ex:
        traceback.print_exc()

def update_queue(poll_result):
    print(f"Updating queue to {repr(poll_result['queue'])}...")
    with open('queue.txt', 'w', encoding='utf-8-sig') as f:
        for item in poll_result['queue']:
            f.write(f'{item}\n')
        global QUEUE
        QUEUE = poll_result['queue']

print('Clearing old files...')
for old_file in os.listdir(os.path.abspath(os.path.dirname(__file__))):
    if old_file.endswith('.txt') or old_file.endswith('.png'):
        os.remove(old_file)

while True:
    try:
        print("Polling for update...")
        poll_result = poll()
        for k, v in poll_result['current'].items():
            if k == 'artwork':
                if not LAST_INDEX:
                    update_artwork(poll_result, k, v)
                else:
                    if LAST_INDEX != f"{poll_result['current']['playlist']}.{poll_result['current']['index']}":
                        update_artwork(poll_result, k, v)
            else:
                if not k in LAST_VALUES:
                    update_text_file(poll_result, k, v)
                else:
                    if LAST_VALUES[k] != v:
                        update_text_file(poll_result, k, v)
        if QUEUE is None:
            update_queue(poll_result)
        else:
            if QUEUE != poll_result['queue']:
                update_queue(poll_result)
        time.sleep(0.5)
    except Exception as ex:
        traceback.print_exc()