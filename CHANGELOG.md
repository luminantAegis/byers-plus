# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.0] Update of Modules and Keystrokes [UoMaK]
### Added:
- Added a response, when !songrequest is called without any query
- Added a custom permission management
    - Inspired by PermissionsEx
    - No code was looked up or copied
    - Completely operable using commands
- Added a utility class for functions, that shouldn't be restricted to just one module
- Added a custom logger for logging each module individually
    - Inspired by log4j
    - Each module can register their own logger
    - Loggers can write to seperate files and can have their own log level
- Added custom SLCB environment for testing without the actual chatbot
    - You'll have to do with my snarky comments though
- Added argument parsing from discord.py
- Added !addbear as alias for !addcan
- Added a custom cooldown management
    - Allows for setting, getting, increasing, decreasing and removing cooldowns for various objects
- Added a last song and current song command
- Added a !search command for checking the song database without requesting anything

### Changed:
- Changed the pull-values script to take the beefweb port from the settings
- Changed logging to use the custom logger of modules
- Changed placeholder resolving to check, if a placeholder exists in the given string
    - If not, nothing will get replaced
- Got rid of all instances of `sys.path` shenanigans
    - Except the one in the main script file, because SLCB doesn't import the script but rather executes it
- You can no longer invoke commands using a longer command than it actually is
    - Example: `!songrequesticanmakethisinfinitelylong`
    - This would mess with the argument detection
- Made it possible to use match placeholders in the response message for songs being on cooldown
- Added the $cooldown_left placeholder for songs being on cooldown to display the time left
- Cleaned up old overlay
- Updated README
- Changed command lookup logic
    - Aliases are now mapped to the respective commands as well
    - This also improves performance if there are a lot of commands
- Changed the number of edits allowed in the fuzzy search parameters

### Fixed:
- !addcan now sets a global cooldown
    - This fixed world hunger and saved canconomy
- If a song doesn't have an album, the pull-values script will now generate a black square as artwork
- Fixed being able to request the same song multiple times by requesting it during the !yes time
- Fixed being able to request the currently playing song
    - This will also place it on cooldown
- Special characters properly get URL-encoded now

## [1.1.0] Update of Encodings and Cooldowns [UoEaC] - 2019-10-05
### Added:
- Placeholders can now resolve settings using the prefix `setting_`
- New settings:
    - `songrequest_hour_requirement`: Minimum hours watched to request a song
    - `songrequest_not_enough_hours_message`: Message for not enough hours
    - `songrequest_yes_timeout`: Timeout for confirmation (-1 to turn off)
    - `songrequest_yes_timeout_message`: Message for doing !yes when it timed out
    - `songrequest_yes_no_request_message`: Message for doing !yes without requesting a song
- More verbose output of the `pull-values.py` script

### Changed:
- The `pull-values.py` script now caches the written contents
    - If a value hasn't changed since the last poll, it won't be written to file again

### Fixed:
- Placeholders are now replaced when sending a message
- The !songrequest command now checks for the amount of hours a user has
- The !yes command gives a proper response now, depending on if the request timed out or no song was requested
- Unicode symbols in song metadata no longer cause the `pull-values.py` script to stop creating files

## [1.0.0] Release - 2019-10-05
### BUNP to release version

## [0.1.0] - 2019-10-03
### First "working" version

## [0.0.1] - 2019-03-26
### Initial commit with boilerplate code required for Byers.