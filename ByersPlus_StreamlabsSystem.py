import datetime
import io
import json
import os
import sys
import traceback

# Byers+ imports
sys.path.append(os.path.dirname(__file__))
from modules import BP_AegiSQL, BP_Commands, BP_Placeholders, BP_Songs, BP_Logging, BP_Permissions, BP_DaveStrider
from modules.BP_Settings import ByersSettings

if "DEBUG" in os.environ:
    from SLCBEnvironment import SLCB_Parent
    Parent = SLCB_Parent()

# TODO: Strife+
# TODO: Fever Mode

class ByersPlus:

    def __init__(self):
        self.main_dir = os.path.abspath(os.path.dirname(__file__))
        self.log_file = os.path.join(self.main_dir, "byersPlus.log")
        self.script_name = "Byers+"
        self.website = "https://gitlab.com/luminantAegis/byers-plus"
        self.description = "Radio Moderation Tools ported to Python"
        self.creator = "cozyGalvinism, luminantAegis"
        self.version = "1.2.0"

    def init(self):
        self.logger = BP_Logging.BP_Logger('Byers+', self.log_file, append_to_default=False, log_callback=self.log, log_level=BP_Logging.BP_Logger.LogLevel.DEBUG)
        BP_Logging.BP_Logger.set_default_logger(self.logger)
        settings_file = os.path.join(self.main_dir, "byersPlus.json")
        db_file = os.path.join(os.path.dirname(__file__), "byers_plus.db")
        self._database = BP_AegiSQL.AegiSQL(db_file)
        try:
            self.settings = ByersSettings(settings_file)
            self._permissions = BP_Permissions.BP_Permissions(self._database)
            self._placeholders = BP_Placeholders.Placeholders(self._database, self)
            self._player = BP_Songs.PlayerControl(self)
            self._cooldowns = BP_DaveStrider.BP_DaveStrider(self._database)
            self._commands = BP_Commands.CommandLoader(self)
        except Exception as ex:
            self.logger.error('Error while loading modules', exception=True)

    def execute(self, data):
        self._commands.handle_commands(data)

    def tick(self):
        pass

    def parse(self, parse_string, userid, username, targetid, targetname, message):
        return self._placeholders.replace_placeholders(parse_string)

    def reload_settings(self, jsonData):
        self.logger.info("Reloading settings...")
        self.settings.reload(jsonData)

    def unload(self):
        self.logger.info("Unloading Byers+...")
        if '_database' in self.__dict__ and self._database:
            self._database.close()

    def script_toggled(self, state):
        self.logger.debug("Toggled Byers+ %s..." % ("on" if state else "off"))
    
    def send_message(self, message, target="", stream=True, whisper=False, discord=False, dm=False):
        resolved = self._placeholders.replace_placeholders(message)
        if stream:
            Parent.SendStreamMessage(resolved)
        if discord:
            Parent.SendDiscordMessage(resolved)
        if whisper and target != "":
            Parent.SendStreamWhisper(target, resolved)
        if dm and target != "":
            Parent.SendDiscordDM(target, resolved)

    def log(self, message):
        if not "Parent" in globals():
            return
        Parent.Log(self.script_name, message)
    
    def add_cooldown(self, command, seconds, user=None):
        if user is None:
            Parent.AddCooldown(self.script_name, command, seconds)
        else:
            Parent.AddUserCooldown(self.script_name, command, user, seconds)
    
    def is_on_cooldown(self, command, user=None):
        if user is None:
            return Parent.IsOnCooldown(self.script_name, command)
        else:
            return Parent.IsOnUserCooldown(self.script_name, command, user)
    
    def get_cooldown_duration(self, command, user=None, pretty_print=False):
        seconds = 0
        if user is None:
            seconds = Parent.GetCooldownDuration(self.script_name, command)
        else:
            seconds = Parent.GetUserCooldownDuration(self.script_name, command, user)
        if pretty_print:
            minutes, seconds = divmod(seconds, 60)
            hours, minutes = divmod(minutes, 60)

            periods = [('hours', hours), ('minutes', minutes), ('seconds', seconds)]
            time_string = ', '.join('{} {}'.format(value, name)
                                    for name, value in periods
                                    if value)
            return time_string
        else:
            return seconds
    
    def send_websocket_event(self, event_name, data):
        Parent.BroadcastWsEvent(event_name, json.dumps(data))
    
    def get_request(self, url, headers={}):
        return Parent.GetRequest(url, headers)
    
    def post_request(self, url, headers={}, content={}, is_json=False):
        return Parent.PostRequest(url, headers, content, is_json)
    
    def get_hours(self, user):
        return Parent.GetHours(user)
    
    def get_display_name(self, user):
        return Parent.GetDisplayName(user)
    
    def has_permission(self, user, permission, info=""):
        return Parent.HasPermission(user, permission, info)

bp = ByersPlus()
ScriptName = bp.script_name
Website = bp.website
Description = bp.description
Creator = bp.creator
Version = bp.version
Init = bp.init
Execute = bp.execute
Tick = bp.tick
Parse = bp.parse
ReloadSettings = bp.reload_settings
Unload = bp.unload
ScriptToggled = bp.script_toggled
