# Byers-Plus

Various radio tools ported to Python.

## Features
* Song database
    * With song requests
* Placeholder management
* Logging
* SQL client
* Command management
* Custom permission management based on PermissionsEx
* Test environment for SLCB
    * Can simulate almost all script features from SLCB

## Requirements

* Solr
    * Needs to be on the same machine without authentication
    * Needs a core with the name `bp_song` and a set schema (data-driven schema can be used, but is not supported!)
* foobar2000 with custom beefweb plugin installed
    * See https://gitlab.com/cozyGalvinism/beefweb
    * This adds supports for queues
    * Beefweb plugin only works on foobar2000!
    
You can index the song list by running `playlist-export.py` with Python 3 (requirements: `requests`). Don't forget to adjust ports.