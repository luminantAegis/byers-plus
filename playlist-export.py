import requests
import json
import math

BEEFWEB_PORT = 8888
SOLR_PORT = 32768
HARD_LIMIT = 500 # set this to -1, if you don't need hard limits

# Grab playlists
playlists = requests.get('http://127.0.0.1:%d/api/playlists' % BEEFWEB_PORT).json()["playlists"]

db = []
for playlist in playlists:
    pl_id = playlist["id"]
    count = playlist["itemCount"]
    if HARD_LIMIT == -1:
        songs = requests.get('http://127.0.0.1:%d/api/playlists/%s/items/0:%d?columns=%%25album%%25,%%25artist%%25,%%25title%%25,%%25track%%20number%%25,%%25list_index%%25' % (BEEFWEB_PORT, pl_id, count)).json()["playlistItems"]["items"]
    else:
        request_amount = math.ceil(count / HARD_LIMIT)
        songs = []
        for i in range(0, request_amount):
            temp_songs = requests.get('http://127.0.0.1:%d/api/playlists/%s/items/%d:%d?columns=%%25album%%25,%%25artist%%25,%%25title%%25,%%25track%%20number%%25,%%25list_index%%25' % (BEEFWEB_PORT, pl_id, i * HARD_LIMIT, HARD_LIMIT)).json()["playlistItems"]["items"]
            print(f'Got {len(temp_songs)} elements...')
            songs.extend(temp_songs)
    for song in songs:
        album = song["columns"][0]
        artist = song["columns"][1]
        title = song["columns"][2]
        track_no = song["columns"][3]
        list_index = song["columns"][4]
        dbo = {
            'album': album,
            'artist': artist,
            'title': title,
            'tracknumber': track_no,
            'playlist': pl_id,
            'playlist_index': list_index,
            'id': f'{pl_id}.{list_index}'
        }
        db.append(dbo)

with open('tracks.json', 'w', encoding='utf-8') as f:
    print('Creating JSON file...')
    json.dump(db, f, indent=2)

with open('tracks.json', 'rb') as f:
    print('Updating Solr index...')
    response = requests.post(f'http://127.0.0.1:{SOLR_PORT}/solr/bp_song/update', json={"delete": {"query": "*:*"}})
    print(f'Deleted old index: {response.text}')
    response = requests.post(f'http://127.0.0.1:{SOLR_PORT}/solr/bp_song/update/json?commit=true', files={'tracks.json': f})
    print(response.text)
print("Thank you for using ViperDrive! It's state of the art.")