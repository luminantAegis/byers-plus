"""
This is not a module, but a simulator of SLCB. 
It contains all functions of the Parent object, as well as 
functions to test messages and certain events.

Disclaimer:
THIS IS NOT A WORKING BOT! I DIDN'T COME UP WITH THE NAMING
OF THESE CLASSES AND I'M ONLY DOING THIS TO MAKE DEBUGGING
LESS OF A BITCH!
"""
import random
import json

class SLCB_Parent:
    """
    Parent class for pretty much all functions
    """

    def __init__(self):
        pass

    # Currency functions

    def AddPoints(self, userid, username, amount, purposely_fail=False):
        """
        ### Adding currency to a single user

        `purposely_fail` is NOT a part of the original function
        """
        print("Adding %d points to %s (%s, purposely_fail: %s)" % (amount, username, userid, str(purposely_fail)))
        return not purposely_fail
    
    def RemovePoints(self, userid, username, amount, purposely_fail=False):
        """
        ### Removing currency from a single user

        `purposely_fail` is NOT a part of the original function
        """
        print("Removing %d points from %s (%s, purposely_fail: %s)" % (amount, username, userid, str(purposely_fail)))
        return not purposely_fail

    def AddPointsAll(self, data, purposely_fail=[]):
        """
        ### Batch Adding
        Yes, this is the original title in the docs

        `data` is a `dict(str, int)`

        `purposely_fail` is NOT a part of the original function and takes keys, that this function should fail for
        """
        fail = []
        for k, v in data.iteritems():
            failing = k in purposely_fail
            print("Batch adding %d points to %s (purposely_fail: %s)" % (v, k, str(failing)))
            if failing:
                fail.append(k)
        return fail
    
    def AddPointsAllAsync(self, data, callback, purposely_fail=[]):
        """
        ### Batch Adding
        Yes, this is the original title in the docs. This function is not truly async, go figure.

        `data` is a `dict(str, int)`

        `callback` is a `func(list(str)) -> None`

        `purposely_fail` is NOT a part of the original function and takes keys, that this function should fail for
        """
        fail = []
        for k, v in data.iteritems():
            failing = k in purposely_fail
            print("Asynchronously batch adding %d points to %s (purposely_fail: %s)" % (v, k, str(failing)))
            if failing:
                fail.append(k)
        callback(fail)
    
    def RemovePointsAll(self, data, purposely_fail=[]):
        """
        ### Batch Removing
        Yes, this is the original title in the docs

        `data` is a `dict(str, int)`

        `purposely_fail` is NOT a part of the original function and takes keys, that this function should fail for
        """
        fail = []
        for k, v in data.iteritems():
            failing = k in purposely_fail
            print("Batch removing %d points from %s (purposely_fail: %s)" % (v, k, str(failing)))
            if failing:
                fail.append(k)
        return fail
    
    def RemovePointsAllAsync(self, data, callback, purposely_fail=[]):
        """
        ### Batch Removing
        Yes, this is the original title in the docs. This function is not truly async, go figure.

        `data` is a `dict(str, int)`

        `callback` is a `func(list(str)) -> None`

        `purposely_fail` is NOT a part of the original function and takes keys, that this function should fail for
        """
        fail = []
        for k, v in data.iteritems():
            failing = k in purposely_fail
            print("Asynchronously batch removing %d points from %s (purposely_fail: %s)" % (v, k, str(failing)))
            if failing:
                fail.append(k)
        callback(fail)
    
    def GetPoints(self, userid, purposely_fail=False):
        """
        ### Retrieving User's Currency

        This function returns 0 for users, that aren't in the database

        `purposely_fail` is NOT a part of the original function
        """
        amount = random.randint(0, 50000) if not purposely_fail else 0
        print("User %s has %d points (purposely_fail: %s)" % (userid, amount, str(purposely_fail)))
        return amount
    
    def GetHours(self, userid, purposely_fail=False):
        """
        ### Retrieving User's Hours Watched

        This function returns 0 for users, that aren't in the database

        `purposely_fail` is NOT a part of the original function
        """
        amount = random.randint(0, 2000) if not purposely_fail else 0
        print("User %s has %d hours (purposely_fail: %s)" % (userid, amount, str(purposely_fail)))
        return amount
    
    def GetRank(self, userid, purposely_fail=False):
        """
        ### Retrieving User's Rank

        This function returns 0 for users, that aren't in the database

        `purposely_fail` is NOT a part of the original function
        """
        amount = random.randint(0, 2000) if not purposely_fail else 0
        print("User %s has a rank of %d (purposely_fail: %s)" % (userid, amount, str(purposely_fail)))
        return amount
    
    def GetTopCurrency(self, top):
        """
        ### Retrieving Top X Users Based on Currency
        Does not sort!
        """
        top_list = {}
        for i in xrange(0, top):
            amount = random.randint(0, 50000)
            top_list["user_%d" % i] = amount
            print("Top currency %d - %s: %d" % (i+1, "user_%d" % i, amount))
        return top_list
    
    def GetTopHours(self, top):
        """
        ### Retrieving Top X Users Based on Hours Watched
        Does not sort!
        """
        top_list = {}
        for i in xrange(0, top):
            amount = random.randint(0, 2000)
            top_list["user_%d" % i] = amount
            print("Top hours %d - %s: %d" % (i+1, "user_%d" % i, amount))
        return top_list
    
    def GetPointsAll(self, userids, purposely_fail=[]):
        """
        ### Batch Retrieving User's Points

        `purposely_fail` is NOT a part of the original function and takes keys, that this function should fail for
        """
        result = {}
        for userid in userids:
            failing = userid in purposely_fail
            amount = 0 if failing else random.randint(0, 50000)
            result[userid] = amount
            print("Batch user %s has %d points (purposely_fail: %s)" % (userid, amount, str(failing)))
        return result
    
    def GetRanksAll(self, userids, purposely_fail=[]):
        """
        ### Batch Retrieving User's Ranks

        `purposely_fail` is NOT a part of the original function and takes keys, that this function should fail for
        """
        result = {}
        for userid in userids:
            failing = userid in purposely_fail
            amount = 0 if failing else random.randint(0, 2000)
            result[userid] = amount
            print("Batch user %s has a rank of %d (purposely_fail: %s)" % (userid, amount, str(failing)))
        return result
    
    def GetHoursAll(self, userids, purposely_fail=[]):
        """
        ### Batch Retrieving User's Hours

        `purposely_fail` is NOT a part of the original function and takes keys, that this function should fail for
        """
        result = {}
        for userid in userids:
            failing = userid in purposely_fail
            amount = 0 if failing else random.randint(0, 2000)
            result[userid] = amount
            print("Batch user %s has %d hours (purposely_fail: %s)" % (userid, amount, str(failing)))
        return result
    
    def GetCurrencyUsers(self, userids, purposely_fail=[]):
        """
        ### Batch Retrieving User's Currency Information

        `purposely_fail` is NOT a part of the original function and takes keys, that this function should fail for
        """
        result = []
        for userid in userids:
            failing = userid in purposely_fail
            if not failing:
                currency = Currency(userid, "generated_name", random.randint(0, 50000), random.randint(0, 2000)*60, random.randint(0, 2000))
                result.append(currency)
            print("Generating currency information for user %s (purposely_fail: %s)" % (userid, str(failing)))
        return result

    # Message functions

    def SendStreamMessage(self, message):
        """
        ### Sending Messages to the Stream Chat
        """
        print("Bot->Stream: %s" % message)
    
    def SendStreamWhisper(self, target, message):
        """
        ### Sending Whispers to the Stream Chat (Only applicable to Twitch)
        """
        print("Bot->%s (Whisper): %s" % (target, message))
    
    def SendDiscordMessage(self, message):
        """
        ### Sending Messages to Discord (Only if the user has set up the bot for Discord)
        """
        print("Bot->Discord: %s" % message)
    
    def SendDiscordDM(self, target, message):
        """
        ### Sending DMs to Users on Discord
        """
        print("Bot->%s (Discord DM): %s" % (target, message))
    
    def BroadcastWsEvent(self, event_name, json_data):
        """
        ### Sending an event to Connected Overlays
        """
        print("Bot->WS (%s): %s" % (event_name, json_data))
    
    # Permission functions

    def HasPermission(self, userid, permission, info, purposely_fail=False):
        print("Permission check for user %s on permission %s with info %s (purposely_fail: %s)" % (userid, permission, info, purposely_fail))
        return not purposely_fail
    
    # Viewer functions

    def GetViewerList(self):
        """
        ### Retrieving the Viewerlist
        """
        amount = random.randint(1, 100)
        viewers = []
        for i in xrange(0, amount):
            username = "user_%d" % random.randint(0, 100)
            viewers.append(username)
            print("Viewer %s on stream" % username)
        return viewers
    
    def GetActiveUsers(self):
        """
        ### Retrieving all Active Users
        """
        amount = random.randint(1, 100)
        viewers = []
        for i in xrange(0, amount):
            username = "user_%d" % random.randint(0, 100)
            viewers.append(username)
            print("Viewer %s is active" % username)
        return viewers
    
    def GetRandomActiveUser(self):
        """
        ### Retrieving a Random Active User
        """
        user = random.choice(self.GetActiveUsers())
        print("Picked random active user %s" % user)
        return user

    def GetDisplayName(self, userid, purposely_fail=False):
        """
        ### Retrieving a User their Display Name
        Engrish is strong on this one

        `purposely_fail` is NOT a part of the original function
        """
        print("Getting display name of %s (purposely_fail: %s)" % (userid, str(purposely_fail)))
        if purposely_fail:
            return None
        return userid
    
    def GetDisplayNames(self, userids, purposely_fail=[]):
        """
        ### Batch retrieving Usernames for the requested UserIds
        This is better

        `purposely_fail` is NOT a part of the original function and takes keys, that this function should fail for
        """
        names = {}
        for userid in userids:
            failing = userid in purposely_fail
            print("Getting display name of %s (purposely_fail: %s)" % (userid, str(purposely_fail)))
            if not failing:
                names[userid] = 'generated_name'
        return names
    
    def AddCooldown(self, script_name, command, seconds):
        """
        ### Adding a Command to the Cooldown Manager
        """
        print("Added a %d second cooldown to %s in %s" % (seconds, command, script_name))
    
    def IsOnCooldown(self, script_name, command, purposely_fail=False):
        """
        ### Checking if the Command is on Cooldown

        `purposely_fail` is NOT a part of the original function
        """
        print("Checking if %s of %s is on cooldown (purposely_fail: %s)" % (command, script_name, str(purposely_fail)))
        return purposely_fail
    
    def GetCooldownDuration(self, script_name, command):
        """
        ### Retrieving the Remaining Cooldown Duration
        """
        amount = random.randint(0, 600)
        print("Getting cooldown duration of %s in %s" % (command, script_name))
        return amount
    
    def AddUserCooldown(self, script_name, command, userid, seconds):
        """
        ### Adding a Usercooldown to a Command
        """
        print("Adding a %d second cooldown to %s in %s for user %s" % (seconds, command, script_name, userid))
    
    def IsOnUserCooldown(self, script_name, command, userid, purposely_fail=False):
        """
        ### Checking if a Command is on Usercooldown
        Consistency is not is strong suit

        `purposely_fail` is NOT a part of the original function
        """
        print("Checking if %s of %s is on cooldown for %s (purposely_fail: %s)" % (command, script_name, userid, str(purposely_fail)))
        return purposely_fail
    
    def GetUserCooldownDuration(self, script_name, command, userid):
        """
        ### Retrieving the Remaining Cooldown Duration
        """
        amount = random.randint(0, 600)
        print("Getting cooldown duration of %s in %s for user %s" % (command, script_name, userid))
        return amount
    
    # OBS functions (not implemeted)

    def SetOBSCurrentScene(self, scene_name, callback=None):
        """
        ### Change your Scene on OBS

        Does not return what OBS would return!
        """
        pass
    
    def SetOBSSourceRender(self, source, render, scene_name=None, callback=None):
        """
        ### Show/Hide a Source in OBS
        """
        pass
    
    def StopOBSStreaming(self, callback=None):
        """
        ### Stop the Stream
        """
        pass

    def GetOBSSpecialSources(self, callback):
        """
        ### Retrieve all Audio Sources
        """
        pass

    def SetOBSVolume(self, source, volume, callback=None):
        """
        ### Control the Volume of an OBS Source
        """
        pass

    def GetOBSMute(self, source, callback=None):
        """
        ### Mute a Specific Source in OBS
        Rather "gets the mute state of a specific OBS scene"
        """
        pass
    
    def SetOBSMute(self, source, mute, callback=None):
        """
        ### Toggle the Mute State of a Specific OBS Source
        """
        pass

    def ToggleOBSMute(self, source, callback=None):
        """
        ### Toggle Mute of a Specific OBS Source
        """
        pass

    # API functions

    def GetRequest(self, url, headers):
        """
        ### GET request
        """
        print("Performing GET request to %s with headers %s" % (url, repr(headers)))
        return "<response>"
    
    def PostRequest(self, url, headers, content, is_json_content=True):
        """
        ### POST request
        """
        print("Performing POST request to %s with headers %s, content %s and %s" % (url, repr(headers), content, "is JSON" if is_json_content else "is not JSON"))
        return "<response>"
    
    def DeleteRequest(self, url, headers):
        """
        ### DELETE request
        """
        print("Performing DELETE request to %s with headers %s" % (url, repr(headers)))
        return "<response>"
    
    def PutRequest(self, url, headers, content, is_json_content=True):
        """
        ### PUT request
        """
        print("Performing PUT request to %s with headers %s, content %s and %s" % (url, repr(headers), content, "is JSON" if is_json_content else "is not JSON"))
        return "<response>"
    
    # Stream functions

    def IsLive(self):
        """
        ### Check If the Stream is Live
        """
        return True
    
    # Gamewisp function

    def GetGwTierLevel(self, user):
        """
        ### Retrieve a User's Gamewisp Sub Tier
        Yeah, right?
        """
        print("Getting Gamewisp tier level of %s" % user)
        return 1
    
    # Misc functions

    def GetRandom(self, min_num, max_num):
        """
        ### Getting a Random number
        Well, what else did you expect?

        Actually works around a critical bug in IronPython, where
        `random.randint()` would not generate numbers above 50
        """
        print("Generating a random number between %d and %d" % (min_num, max_num))
        return random.randint(min_num, max_num)
    
    def GetStreamingService(self):
        """
        ### Retrieve the Currency Platform that the Chatbot is being used on
        How to make a long headline
        """
        return "YouTube/Twitch... pick one"
    
    def GetChannelName(self):
        """
        ### Getting the Stream's Channel Name (Only applicable to Twitch)
        Thanks Ankhheart
        """
        return "Channel name"
    
    def GetCurrencyName(self):
        """
        ### Retrieving the Stream's Currency Name
        """
        return "Boondollars"
    
    def Log(self, script_name, message):
        """
        ### Logging Information to the Bot's Log Window
        The most insufficient log function I have ever seen
        Seriously, write it to a file or something...
        The view of the log is bad as well...
        """
        print("[%s] %s" % (script_name, message))
    
    def PlaySound(self, file, volume, purposely_fail=False):
        """
        ### Attempt to Play a sound if Possible
        I don't know if it's possible, you tell me
        """
        print("Trying to play sound %s with volume %s (purposely_fail: %s)" % (file, volume, str(purposely_fail)))
        return not purposely_fail
    
    def GetQueue(self, max_amount):
        """
        ### Retrieve X amount of Users that are in the Queue at the moment
        """
        result = {}
        for i in xrange(0, max_amount):
            result[i] = "generated_user"
        print("Loading up to %d users from the queue" % max_amount)
        return result
    
    # Song functions

    def GetSongQueue(self, max_amount):
        """
        ### Retrieve the next X amount of songs in the queue
        """
        result = []
        for i in xrange(0, max_amount):
            result.append(Song("Title", "me", "also me", "ID%d" % i, "http://127.0.0.1"))
        print("Getting up to %d songs from the queue" % max_amount)
        return result
    
    def GetSongPlaylist(self, max_amount):
        """
        ### Retrieve the next X amount of songs in the playlist
        """
        result = []
        for i in xrange(0, max_amount):
            result.append(Song("Title", "me", "also me", "ID%d" % i, "http://127.0.0.1"))
        print("Getting up to %d songs from the playlist" % max_amount)
        return result
    
    def GetNowPlaying(self):
        """
        ### Get the Current song that's playing
        """
        print("Getting currently playing song")
        return {"Song": "Requested By"}


class Currency:

    def __init__(self, UserId, UserName, Points, TimeWatched, Rank):
        self.UserId = UserId
        self.UserName = UserName
        self.Points = Points
        self.TimeWatched = TimeWatched
        self.Rank = Rank

class Song:

    def __init__(self, Title, RequestedBy, RequestedByName, ID, URL):
        self.Title = Title
        self.RequestedBy = RequestedBy
        self.RequestedByName = RequestedByName
        self.ID = ID
        self.URL = URL

class Message:

    def __init__(self, message, userid, user, raw=None, is_chat=True, is_raw=False, is_twitch=False, is_yt=True, is_mixer=False, is_discord=False, is_whisper=False):
        self.Message = message
        self.User = userid
        self.UserName = user
        self.RawData = raw
        self. is_chat = is_chat
        self.is_raw = is_raw
        self.is_twitch = is_twitch
        self.is_yt = is_yt
        self.is_mixer = is_mixer
        self.is_discord = is_discord
        self.is_whisper = is_whisper
    
    def IsChatMessage(self):
        return self.is_chat
    
    def IsRawData(self):
        return self.is_raw
    
    def IsFromTwitch(self):
        return self.is_twitch
    
    def IsFromYoutube(self):
        return self.is_yt
    
    def IsFromMixer(self):
        return self.is_mixer
    
    def IsFromDiscord(self):
        return self.is_discord
    
    def IsWhisper(self):
        return self.is_whisper
