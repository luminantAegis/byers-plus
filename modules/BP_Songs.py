import os
import io
import json
import urllib
from modules.BP_AegiSQL import AegiSQL

# Song search: SELECT * FROM song WHERE MATCH(album,artist,title) AGAINST ('query' IN NATURAL LANGUAGE MODE);
# http://127.0.0.1:32769/solr/bp_song/select?defType=edismax&q=zeanpuryu~&qf=title%5E2&stopwords=true
class SongDB:

    def __init__(self, module):
        self._cache = []
        self.module = module
        self._database = module._database # type: AegiSQL
        self.solr_port = module.settings.solr_port
    
    def solr_query(self, search_str="", fuzzy=True):
        """Uses Solr as backend search engine"""
        final = ""
        if fuzzy:
            blacklist = ['AND', 'OR', '&&', '||', '+', '-', '(', ')']
            for word in search_str.split():
                if word in blacklist or ':' in word:
                    final = final + word + " "
                else:
                    final = final + word + "~1 "
            final = final.rstrip()
        else:
            final = search_str
        final = urllib.quote(final)
        response = self.module.get_request("http://127.0.0.1:%d/solr/bp_song/select?defType=edismax&q=%s&qf=title%%5E2&stopwords=true" % (self.module.settings.solr_port, final))
        response_obj = json.loads(json.loads(response)['response'])
        songs = []
        if 'response' in response_obj and 'docs' in response_obj['response']:
            for doc in response_obj['response']['docs']:
                songs.append(doc)
        return songs


class PlayerControl:

    def __init__(self, module):
        self._database = SongDB(module)
        self.module = module
        self.module._database.execute("""
        CREATE TABLE IF NOT EXISTS bp_song_history (
            requestant TEXT,
            requestant_display TEXT,
            date_requested TEXT,
            album_requested TEXT,
            artist_requested TEXT,
            title_requested TEXT
        );
        """)
        self.last_requested = None
        self.update_thread = None
    
    def request(self, query, advanced=False):
        results = self._database.solr_query(query, not advanced)
        # For ease, we'll take the first one
        if results:
            best_match = results[0]
            return best_match
        else:
            return None
    
    def play(self, match):
        pl_index = int(match['playlist_index'])-1
        secs_resp = json.loads(json.loads(self.module.get_request('http://127.0.0.1:%d/api/playlists/%s/items/%s:%d?columns=%%length_seconds%%' % (self.module.settings.beefweb_port, match['playlist'], pl_index, 1), {}))['response'])
        self.module.post_request('http://127.0.0.1:%d/api/queue/add/%s/%s' % (self.module.settings.beefweb_port, match['playlist'], pl_index))
        self.last_requested = match
        return int(secs_resp['playlistItems']['items'][0]['columns'][0])
    
    def update_ui(self):
        url = 'http://127.0.0.1:%d/api/player?columns=%%25album%%25,%%25artist%%25,%%25title%%25,%%25playback_time_remaining_seconds%%25' % self.module.settings.beefweb_port
        state = self.module.get_request(url)
        state_obj = json.loads(json.loads(state)['response'])
        current = {'album': state_obj['player']['activeItem']['columns'][0], 'artist': state_obj['player']['activeItem']['columns'][1], 'title': state_obj['player']['activeItem']['columns'][2], 'remaining': state_obj['player']['activeItem']['columns'][3]}
        queue = json.loads(json.loads(self.module.get_request('http://127.0.0.1:%d/api/queue' % self.module.settings.beefweb_port))['response'])['queue']
        queue_list = []
        for item in queue:
            song_res = self.module.get_request('http://127.0.0.1:%d/api/playlists/%s/items/%d:1?columns=%%25title%%25' % (self.module.settings.beefweb_port, item['playlistId'], item['itemIndex']))
            queue_list.append(json.loads(json.loads(song_res)['response'])['playlistItems']['items'][0]['columns'][0])
        update = {'current': current, 'queue': queue_list}
        self.module.send_websocket_event('EVENT_UPDATE_UI', update)
    
    def undo(self):
        if self.last_requested is None:
            return
        # Remove last added song from queue (highest index)
    
    @property
    def last_song(self):
        obs_dir = os.path.join(self.module.main_dir, 'obs')
        album, title = os.path.join(obs_dir, "album_old.txt"), os.path.join(obs_dir, "title_old.txt")
        last = None
        if os.path.exists(album) and os.path.exists(title):
            last = {}
            with io.open(album, 'r', encoding='utf-8-sig') as f:
                last['album'] = f.read()
            with io.open(title, 'r', encoding='utf-8-sig') as f:
                last['title'] = f.read()
        return last

    @property
    def current_song(self):
        url = 'http://127.0.0.1:%d/api/player?columns=%%25album%%25,%%25title%%25' % self.module.settings.beefweb_port
        player_obj = json.loads(json.loads(self.module.get_request(url))['response'])
        album = player_obj['player']['activeItem']['columns'][0]
        title = player_obj['player']['activeItem']['columns'][1]
        playlist = player_obj['player']['activeItem']['playlistId']
        playlist_index = int(player_obj['player']['activeItem']['index'])
        return {'album': album, 'title': title, 'playlist': playlist, 'index': playlist_index}