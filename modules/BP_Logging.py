from datetime import datetime
import traceback
import io

class BP_Logger:

    default_logger = None

    def __init__(self, module_name, file_path=None, append_to_default=True, log_callback=None, log_level=(3, "ERROR")):
        self.module_name = module_name
        self.append_to_default = append_to_default and (BP_Logger.default_logger and self.module_name != BP_Logger.default_logger.module_name)
        self.log_callback = log_callback
        self.file_path = file_path
        self.log_level = log_level

    def log(self, message, level, exception=False, module=None):
        formatted = ""
        if exception:
            formatted = "[{date}] [{module}] [{level}] {message}:\n{exception}\n".format(date=datetime.now(), module=self.module_name if module is None else module, level=level[1], message=message, exception=traceback.format_exc())
        else:
            formatted = "[{date}] [{module}] [{level}] {message}\n".format(date=datetime.now(), module=self.module_name if module is None else module, level=level[1], message=message)
        
        if self.append_to_default:
            BP_Logger.default_logger.log(message, level, exception, module=self.module_name)
        else:
            print(formatted)
        if self.log_level[0] > level[0]:
            return
        if self.file_path:
            with io.open(self.file_path, 'a', encoding='utf-8-sig') as f:
                f.write(unicode(formatted, encoding='utf-8-sig'))
        if self.log_callback:
            self.log_callback(formatted)

    def info(self, message, exception=False):
        self.log(message, BP_Logger.LogLevel.INFO, exception)

    def warn(self, message, exception=False):
        self.log(message, BP_Logger.LogLevel.WARN, exception)

    def error(self, message, exception=False):
        self.log(message, BP_Logger.LogLevel.ERROR, exception)

    def fatal(self, message, exception=False):
        self.log(message, BP_Logger.LogLevel.FATAL, exception)

    def debug(self, message, exception=False):
        self.log(message, BP_Logger.LogLevel.DEBUG, exception)

    @classmethod
    def set_default_logger(cls, logger):
        cls.default_logger = logger
    

    class LogLevel:

        DEBUG = (0, "DEBUG")
        INFO = (1, "INFO")
        WARN = (2, "WARN")
        ERROR = (3, "ERROR")
        FATAL = (4, "FATAL")