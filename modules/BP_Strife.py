"""
You could call it Strife++

Reflects more of an actual game

This finally puts my game dev knowledge to use!
"""
import random
import datetime
from enum import Enum
from threading import Thread
import time

class Enemy:

    def __init__(self, enemy_name="Shale Imp", enemy_hp=10, minimum_payout=25, strength_min=1, strength_max=3, crit_chance=15, strife_log=None):
        """
        Instantiates a new enemy with given values

        :param enemy_name: Enemy name used for messages
        :param enemy_hp: Loose difficulty rating
        :param min_users: Minimum users required for this enemy to be encountered
        """
        self.enemy_name = enemy_name
        self.max_enemy_hp = enemy_hp
        self.current_hp = self.max_enemy_hp
        self.minimun_payout = minimum_payout
        self.strength_min = strength_min
        self.strength_max = strength_max
        self.crit_chance = crit_chance
        self.dead = False
        self.strife_log = strife_log
    
    def hit(self, user):
        """Damages users"""
        missed = random.randrange(0, 100) == 50
        if missed:
            return (0, False, False, missed)
        damage = random.randrange(self.strength_min, self.strength_max)
        crit = random.randint(1, 100) + 5 < self.crit_chance
        dead = user.damage(damage if not crit else damage*2)
        return (damage, crit, dead, missed)
    
    def damage(self, damage):
        """Damages enemy"""
        if self.current_hp - damage <= 0:
            self.current_hp = 0
            self.dead = True
        else:
            self.current_hp = self.current_hp - damage
        return self.dead

class User:

    def __init__(self, username="", hp=10, strength_min=1, strength_max=3, crit_chance=15, strife_log=None):
        self.max_hp = hp
        self.current_hp = self.max_hp
        self.strength_min = strength_min
        self.strength_max = strength_max
        self.crit_chance = crit_chance
        self.username = username
        self.dead = False
        self.strife_log = strife_log
    
    def hit(self, enemy):
        missed = random.randrange(0, 100) == 50
        if missed:
            return (0, False, False, missed)
        damage = random.randrange(self.strength_min, self.strength_max)
        crit = random.randint(1, 100) + 5 < self.crit_chance
        dead = enemy.damage(damage if not crit else damage*2)
        return (damage, crit, dead, missed)
    
    def damage(self, damage):
        if self.current_hp -damage <= 0:
            self.current_hp = 0
            self.dead = False
        else:
            self.current_hp = self.current_hp - damage
        return self.dead

class StrifeLog:
    """Logs strife info"""

    def __init__(self, on_log=None):
        self.strife_date = datetime.datetime.now()
        self.strife_participants = []
        self.log = []
        self.on_log = on_log
    
    def start(self, participants):
        self.strife_participants = participants
        self.log_action('Strife started on %s with %d participants!' % (str(self.strife_date), len(self.strife_participants)))
        self.log_action('That being said... STRIFE!')
    
    def log_action(self, message):
        if self.on_log is not None:
            self.on_log('[%s] %s' % (str(datetime.datetime.now()), message))
        self.log.append('[%s] %s' % (str(datetime.datetime.now()), message))
    
    def round_start(self, round):
        self.log_action('Round %d begins...' % round)
    
    def round_end(self, round):
        self.log_action('Round %d ended!' % round)
    
    def hit(self, attacker, attacked, damage, crit=False, dead=False, missed=False):
        if missed:
            self.log_action('%s totally missed %s... How embarassing.' % (attacker, attacked))
        else:
            if dead:
                if crit:
                    self.log_action('%s dealt a devastating finishing blow to %s (%d damage)!' % (attacker, attacked, damage))
                else:
                    self.log_action('%s dealt the finishing blow to %s (%d damage)!' % (attacker, attacked, damage))
            else:
                self.log_action('%s%s aggressed %s for %d damage...' % ('CRITICAL HIT! ' if crit else '', attacker, attacked, damage))
    
    def end(self, won=False, user_rewards={}, all_dead=False, perfect=False):
        if won:
            if perfect:
                self.log_action('Strife executed perfectly! No one took any damage!')
            else:
                self.log_action('Strife won!')
        else:
            if all_dead:
                self.log_action('Everyone was wiped out! What a disaster... Scratch this session.')
            else:
                self.log_action('Strife over. The enemy fled.')
    
    def __str__(self):
        full_log = ""
        for log_entry in self.log:
            full_log = full_log + log_entry + "\n"
        return full_log

class StrifeGame:
    """
    10/10 would play again - IGN
    """

    def __init__(self, initiator, max_rounds=1):
        self.max_rounds=max_rounds
        self.users = {}
        self.log = StrifeLog(on_log=self.console_log)
        self.initiator = initiator
        self.add_user(initiator)
    
    def console_log(self, message):
        print(message)

    def add_user(self, username):
        if not username in self.users.keys():
            self.users[username] = User(username)
            return True
        return False
    
    def alive_users(self):
        alive = []
        for user in self.users.values():
            if not user.dead:
                alive.append(user)
        return alive

    def generate_enemy(self):
        return Enemy()

    def strife(self):
        self.log.start(self.users)
        enemy = self.generate_enemy()
        strife_end = False
        perfect = True
        for r in range(1, self.max_rounds + 1):
            self.log.round_start(r)
            for user in self.alive_users():
                info_enemy = enemy.hit(user)
                if info_enemy[3] == False:
                    perfect = False
                self.log.hit(enemy.enemy_name, user.username, info_enemy[0], info_enemy[1], info_enemy[2], info_enemy[3])
            for user in self.alive_users():
                info_user = user.hit(enemy)
                self.log.hit(user.username, enemy.enemy_name, info_user[0], info_user[1], info_user[2], info_user[3])
                if enemy.dead:
                    strife_end = True
                    break
            self.log.round_end(r)
            if strife_end:
                break
        all_dead = len(self.alive_users()) == 0
        self.log.end(won=enemy.dead, user_rewards={}, all_dead=all_dead, perfect=perfect)
        return enemy.dead

class StrifeStatus(Enum):
    AVAILABLE = 1
    LFG = 2
    IN_PROGRESS = 4
    ON_COOLDOWN = 8

class StrifeManager:

    def __init__(self, lfg_duration=60, cooldown=10, on_win=None, on_lose=None):
        self.strife_status = StrifeStatus.AVAILABLE
        self.current_strife = None
        self.lfg_thread = None
        self.lfg_duration = lfg_duration
        self.cooldown = cooldown
        self.on_win = on_win
        self.on_lose = on_lose
    
    def execute_strife(self):
        time.sleep(self.lfg_duration)
        self.strife_status = StrifeStatus.IN_PROGRESS
        won = self.current_strife.strife()
        if won:
            if self.on_win != None:
                self.on_win()
        else:
            if self.on_lose != None:
                self.on_lose()
        self.strife_status = StrifeStatus.ON_COOLDOWN
        time.sleep(self.cooldown)
        self.strife_status = StrifeStatus.AVAILABLE
    
    def open_or_join(self, user):
        if self.strife_status == StrifeStatus.AVAILABLE:
            # Initiate!
            self.current_strife = StrifeGame(user)
            self.strife_status = StrifeStatus.LFG
            self.lfg_thread = Thread(target=self.execute_strife)
            self.lfg_thread.start()
            return True
        elif self.strife_status == StrifeStatus.LFG:
            # Join!
            self.current_strife.add_user(user)
            return True
        return False