"""
Handles placeholders of Byers+
"""
from modules.BP_Logging import BP_Logger

class Placeholders:
    """
    Handles placeholders
    """

    def __init__(self, database, module):
        self.logger = BP_Logger('Placeholders')
        self.module = module
        self._database = database
        self._database.execute("""
        CREATE TABLE IF NOT EXISTS bp_placeholders (
            key TEXT PRIMARY KEY,
            value TEXT
        );
        """)
        self._database.commit()
    
    def set_placeholder(self, placeholder, value):
        """Sets a placeholder to a certain value"""
        has_value = self.get_placeholder(placeholder) is not None
        if has_value:
            self.logger.debug('Updating placeholder %s to %s' % (str(placeholder), str(value)))
            self._database.execute("UPDATE bp_placeholders SET value=? WHERE key=?", params=(str(value), placeholder,))
        else:
            self.logger.debug('Creating new placeholder %s with initial value %s' % (str(placeholder), str(value)))
            self._database.execute("INSERT INTO bp_placeholders VALUES (?, ?)", params=(placeholder, str(value),))
        self._database.commit()
    
    def get_placeholder(self, placeholder):
        """Gets the value of a placeholder"""
        placeholder_val = self._database.execute("SELECT value FROM bp_placeholders WHERE key=?", params=(placeholder,)).fetchone()
        if placeholder_val:
            self.logger.debug('Requested placeholder %s (value: %s)' % (str(placeholder), str(placeholder_val[0])))
            return placeholder_val[0]
        self.logger.debug('Requested placeholder %s not found' % str(placeholder))
        return None
    
    def increase_placeholder(self, placeholder):
        """Only use this on counter placeholders!"""
        currentValue = self.get_placeholder(placeholder)
        if currentValue is None:
            self.set_placeholder(placeholder, "1")
            return 1
        else:
            curr = int(currentValue) + 1
            self.set_placeholder(placeholder, curr)
            return curr
    
    def replace_placeholders(self, string):
        """Replaces all Byers+ placeholders in a string"""
        placeholders = self._database.execute("SELECT * FROM bp_placeholders").fetchall()
        for placeholder in placeholders:
            placeholder_str = "$%s" % placeholder[0]
            if placeholder_str not in string:
                continue
            self.logger.debug('Replacing %s with %s in string %s' % (placeholder_str, str(placeholder[1]), string))
            string = string.replace("%s" % placeholder_str, placeholder[1])
        for k, v in self.module.settings.__dict__.iteritems():
            placeholder_str = "$setting_%s" % k
            if placeholder_str not in string:
                continue
            self.logger.debug('Replacing %s with %s in %s' % (placeholder_str, str(v), string))
            string = string.replace(placeholder_str, str(v))
        return string
