from modules.BP_Utils import TopoSort
from modules.BP_Logging import BP_Logger

class BP_Permissions:
    """
    A more detailed approach to permissions
    """

    def __init__(self, database):
        self.logger = BP_Logger('Permissions')
        self.database = database
        self.logger.info('Initializing permissions module...')
        self.database._cursor.execute("""
        CREATE TABLE IF NOT EXISTS bp_permissions_groups (
            group_name TEXT PRIMARY KEY,
            display_name TEXT,
            rank INTEGER
        );
        """)
        self.database.execute("""
        CREATE TABLE IF NOT EXISTS bp_permissions_groups_permissions (
            group_name TEXT,
            permission TEXT,
            FOREIGN KEY(group_name) REFERENCES bp_permissions_groups(group_name)
        );
        """)
        self.database.execute("""
        CREATE TABLE IF NOT EXISTS bp_permissions_groups_inheritances (
            group_name TEXT,
            other_group TEXT,
            FOREIGN KEY(group_name) REFERENCES bp_permissions_groups(group_name),
            FOREIGN KEY(other_group) REFERENCES bp_permissions_groups(group_name)
            PRIMARY KEY(group_name, other_group)
        );
        """)
        self.database.execute("""
        CREATE TABLE IF NOT EXISTS bp_permissions_users_groups (
            userid TEXT,
            group_name TEXT,
            FOREIGN KEY(group_name) REFERENCES bp_permissions_groups(group_name),
            PRIMARY KEY(userid, group_name)
        );
        """)
        self.database.execute("""
        CREATE TABLE IF NOT EXISTS bp_permissions_users_permissions (
            userid TEXT,
            permission TEXT
        );
        """)
        self.database.execute("""
        CREATE TABLE IF NOT EXISTS bp_permissions_history (
            source TEXT,
            userid TEXT,
            permission TEXT,
            action TEXT,
            date INTEGER
        );
        """)
        self.database.commit()

    @property
    def groups(self):
        groups = []
        sql_groups = self.database.execute("SELECT * FROM bp_permissions_groups;").fetchall()
        for sql_group in sql_groups:
            group_name = sql_group[0]
            groups.append(BP_Permissions.Group(self, group_name).get())
        return groups

    class Group:

        def __init__(self, pb, group_name):
            self.pb = pb
            self.group_name = group_name
            self.shallow = True
        
        def get(self):
            sql_group = self.pb.database.execute("SELECT * FROM bp_permissions_groups WHERE group_name = ?;", (self.group_name,)).fetchone()
            if sql_group is not None:
                self.rank = sql_group[2]
                self.shallow = False
                return self
            return None
        
        def create(self, rank=None):
            try:
                self.pb.database.execute("INSERT INTO bp_permissions_groups (group_name, rank) VALUES (?, ?);", (self.group_name, rank))
                self.pb.database.commit()
                self.pb.logger.info("Added group %s" % self.group_name)
                return True
            except:
                self.pb.logger.error("Error while trying to add group", exception=True)
                return False
        
        def delete(self):
            try:
                self.pb.database.execute("DELETE FROM bp_permissions_groups_permissions WHERE group_name = ?;", (self.group_name,))
                self.pb.database.execute("DELETE FROM bp_permissions_groups_inheritances WHERE group_name = ? OR other_group = ?;", (self.group_name, self.group_name))
                self.pb.database.execute("DELETE FROM bp_permissions_groups WHERE group_name = ?;", (self.group_name,))
                self.pb.database.commit()
                self.pb.logger.info("Deleted group %s" % self.group_name)
                return True
            except:
                self.pb.logger.error("Error while trying to remove group", exception=True)
                return False
        
        def add_inheritance(self, inheritance):
            try:
                inherit_lu = BP_Permissions.Group(self.pb, inheritance).get()
                if inherit_lu is not None:
                    self.pb.database.execute("INSERT INTO bp_permissions_groups_inheritances VALUES (?, ?);", (self.group_name, inheritance))
                    self.pb.database.commit()
                    self.pb.logger.info("Adding inheritance %s to group %s" % (inheritance, self.group_name))
                    return True
                return False
            except:
                self.pb.logger.error("Error while trying to add inheritance", exception=True)
                return False
        
        def remove_inheritance(self, inheritance):
            try:
                self.pb.database.execute("DELETE FROM bp_permissions_groups_inheritances WHERE group_name = ? AND other_group = ?;", (self.group_name, inheritance))
                self.pb.database.commit()
                self.pb.logger.info("Deleted inheritance %s from group %s" % (inheritance, self.group_name))
                return True
            except:
                self.pb.logger.error("Error while trying to remove inheritance", exception=True)
                return False
        
        @property
        def inheritances(self):
            inheritances = []
            sql_group_inheritances = self.pb.database.execute("SELECT other_group FROM bp_permissions_groups_inheritances WHERE group_name = ?;", (self.group_name,)).fetchall()
            for sql_group_inheritance in sql_group_inheritances:
                inheritances.append(BP_Permissions.Group(self.pb, sql_group_inheritance[0]).get())
            return inheritances

        @property
        def permissions(self):
            permissions = []
            sql_group_permissions = self.pb.database.execute("SELECT permission FROM bp_permissions_groups_permissions WHERE group_name = ?;", (self.group_name,)).fetchall()
            for sql_group_permission in sql_group_permissions:
                permissions.append(sql_group_permission[0])
            return permissions
        
        @property
        def users(self):
            sql_users = self.pb.database.execute("SELECT userid FROM bp_permissions_users_groups WHERE group_name = ?;", (self.group_name,)).fetchall()
            users = []
            for sql_user in sql_users:
                users.append(BP_Permissions.User(self.pb, sql_user[0]))
            return users
        
        @property
        def effective_permissions(self):
            groups = self.pb.groups
            permission_list = []
            
            def get_dependencies(group):
                def filter_group(grp):
                    inheritances = group.inheritances
                    return grp.group_name in inheritances
                return filter(filter_group, groups)
            
            effective_groups = TopoSort.sort([self], get_dependencies)
            for eff_group in effective_groups:
                eff_perms = eff_group.permissions
                for perm in eff_perms:
                    if perm in permission_list:
                        continue
                    # is negated?
                    if perm.startswith('-'):
                        # is positive version in list?
                        if perm[1:] in permission_list:
                            # remove positive version
                            permission_list.remove(perm[1:])
                    else:
                        # is negative version in list
                        if "-%s" % perm in permission_list:
                            permission_list.remove("-%s" % perm)
                    permission_list.append(perm)
            return permission_list

        def has_permission(self, permission):
            permissions = self.effective_permissions
            perm_exist = permission in permissions
            negative_exists = "-%s" % permission in permissions
            return perm_exist or (not negative_exists and default)

        def give_permission(self, permission):
            try:
                self.pb.database.execute("INSERT INTO bp_permissions_groups_permissions VALUES (?, ?);", (self.group_name, permission))
                self.pb.database.commit()
                self.pb.logger.info("Added permission %s to group %s" % (permission, self.group_name))
                return True
            except:
                self.pb.logger.error("Error while trying to add permission", exception=True)
                return False

        def revoke_permission(self, permission):
            try:
                self.pb.database.execute("DELETE FROM bp_permissions_groups_permissions WHERE group_name = ? AND permission = ?;", (self.group_name, permission))
                self.pb.database.commit()
                self.pb.logger.info("Removed permission %s from group %s" % (permission, self.group_name))
                return True
            except:
                self.pb.logger.error("Error while trying to remove permission", exception=True)
                return False

        def __str__(self):
            return self.group_name
        
        def __repr__(self):
            return self.group_name
    
    
    class User:

        def __init__(self, permissions_main, userid):
            self.pb = permissions_main
            self.userid = userid
        
        @property
        def groups(self):
            sql_users = self.pb.database.execute("SELECT group_name FROM bp_permissions_users_groups WHERE userid = ?;", (self.userid,)).fetchall()
            groups = []
            for sql_group in sql_users:
                groups.append(BP_Permissions.Group(self.pb, sql_group[0]).get())
            return groups
        
        @property
        def permissions(self):
            sql_permissions = self.pb.database.execute("SELECT permission FROM bp_permissions_users_permissions WHERE userid = ?;", (self.userid,)).fetchall()
            permissions = []
            for permission in sql_permissions:
                permissions.append(permission[0])
            return permissions
        
        @property
        def effective_permissions(self):
            groups = self.groups
            own_permissions = self.permissions
            permission_list = []

            def get_dependencies(group):
                def filter_group(grp):
                    inheritances = group.inheritances
                    return grp.group_name in inheritances
                return filter(filter_group, groups)

            effective_groups = TopoSort.sort(groups, get_dependencies)
            for eff_group in effective_groups:
                eff_perms = eff_group.effective_permissions
                for perm in eff_perms:
                    if perm in permission_list:
                        continue
                    # is negated?
                    if perm.startswith('-'):
                        # is positive version in list?
                        if perm[1:] in permission_list:
                            # remove positive version
                            permission_list.remove(perm[1:])
                    else:
                        # is negative version in list
                        if "-%s" % perm in permission_list:
                            permission_list.remove("-%s" % perm)
                    permission_list.append(perm)
            for perm in own_permissions:
                if perm in permission_list:
                    continue
                # is negated?
                if perm.startswith('-'):
                    # is positive version in list?
                    if perm[1:] in permission_list:
                        # remove positive version
                        permission_list.remove(perm[1:])
                else:
                    # is negative version in list
                    if "-%s" % perm in permission_list:
                        permission_list.remove("-%s" % perm)
                # does permission exist already?
                permission_list.append(perm)
            return permission_list
        
        def add_group(self, group):
            group = BP_Permissions.Group(self.pb, group).get()
            if group:
                try:
                    self.pb.database.execute("INSERT INTO bp_permissions_users_groups VALUES (?, ?);", (self.userid, group.group_name))
                    self.pb.database.commit()
                    self.pb.logger.info("Added user %s to group %s" % (self.userid, group.group_name))
                    return True
                except:
                    self.pb.logger.error("Error while trying to add group to user", exception=True)
                    return False
            return False
        
        def remove_group(self, group):
            try:
                self.pb.database.execute("DELETE FROM bp_permissions_users_groups WHERE userid = ? AND group_name = ?;", (self.userid, group))
                self.pb.database.commit()
                self.pb.logger.info("Removed user %s from group %s" % (self.userid, group.group_name))
                return True
            except:
                self.pb.logger.error("Error while trying to remove group from user", exception=True)
                return False
        
        def has_permission(self, permission, default=False):
            permissions = self.effective_permissions
            perm_exist = permission in permissions
            negative_exists = "-%s" % permission in permissions
            return perm_exist or (not negative_exists and default)

        def give_permission(self, permission):
            try:
                self.pb.database.execute("INSERT INTO bp_permissions_users_permissions VALUES (?, ?);", (self.userid, permission))
                self.pb.database.commit()
                self.pb.logger.info("Added permission %s to user %s" % (permission, self.userid))
                return True
            except:
                self.pb.logger.error("Error while trying to add permission to user", exception=True)
                return False

        def revoke_permission(self, permission):
            try:
                self.pb.database.execute("DELETE FROM bp_permissions_users_permissions WHERE userid = ? AND permission = ?;", (self.userid, permission))
                self.pb.database.commit()
                self.pb.logger.info("Removed permission %s from user %s" % (permission, self.userid))
                return True
            except:
                self.pb.logger.error("Error while trying to remove permission from user", exception=True)
                return False