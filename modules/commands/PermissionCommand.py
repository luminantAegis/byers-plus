"""
This contains all permission commands
"""
from modules.commands.BP_CommandBase import BaseCommand
from modules.BP_Permissions import BP_Permissions

# TODO: Add parents command
# TODO: Remove parents command
class PermissionBaseCommand(BaseCommand):

    def __init__(self, permissions, placeholders, cooldowns, aliases=[]):
        super(PermissionBaseCommand, self).__init__(permissions, placeholders, cooldowns, aliases)
        self.cmd_string = "$bpp"
        self.aliases = ['$perm']
    
    def permission_check(self, module, user, permission, default=False):
        return module.has_permission(user.userid, "editor") or user.has_permission(permission)

    def execute(self, message, module):
        args = message.parameters

        if not args:
            module.send_message("There literally is nothing here...")
            return
        
        if len(args) == 1:
            if args[0].lower() == "me":
                if not self.permission_check(module, message.permissions_user, 'bp_permissions.view.own', default=True):
                    module.send_message("Sorry, you can't use this command!")
                    return
                perm_user = BP_Permissions.User(module._permissions, message.user)
                permissions = perm_user.effective_permissions
                permissions_str = ', '.join(permissions)
                display_name = module.get_display_name(message.user)
                if permissions:
                    response = "You have the following permissions: %s" % (permissions_str)
                else:
                    response = "You currently don't have any special permissions."
                module.send_message(response)
                return
            if args[0].lower() == "groups":
                if not self.permission_check(module, message.permissions_user, 'bp_permissions.view.groups'):
                    module.send_message("Sorry, you can't use this command!")
                    return
                groups = module._permissions.groups
                groups_str = ', '.join(groups)
                if groups:
                    response = "Existing groups: %s" % groups_str
                else:
                    response = "There are no defined groups."
                module.send_message(response)
                return

        if len(args) == 2:
            if args[0].lower() == "user":
                if not self.permission_check(module, message.permissions_user, 'bp_permissions.view.others'):
                    module.send_message("Sorry, you can't use this command!")
                    return
                userid = args[1]
                perm_user = BP_Permissions.User(module._permissions, userid)
                permissions = perm_user.effective_permissions
                permissions_str = ', '.join(permissions)
                display_name = module.get_display_name(userid)
                if permissions:
                    response = "User %s has the following permissions: %s" % (display_name, permissions_str)
                else:
                    response = "User %s currently has no special permissions."
                module.send_message(response)
                return
            if args[0].lower() == "group":
                if not self.permission_check(module, message.permissions_user, 'bp_permissions.view.groups'):
                    module.send_message("Sorry, you can't use this command!")
                    return
                group = args[1]
                perm_group = BP_Permissions.Group(module._permissions, group).get()
                if not perm_group:
                    module.send_message("I couldn't find a group with that name.")
                    return
                permissions = perm_group.effective_permissions
                permissions_str = ', '.join(permissions)
                inheritances = perm_group.inheritances
                inheritances_str = ', '.join(inheritances)
                if permissions:
                    if inheritances:
                        response = "Group %s inherits from the following groups: %s and has the following permissions: %s" % (group, inheritances_str, permissions_str)
                    else:
                        response = "Group %s inherits permissions from no groups and has the following permissions: %s" % (group, permissions_str)
                else:
                    if inheritances:
                        response = "Group %s inherits from the following groups: %s and has no special permissions." % (group, inheritances_str)
                    else:
                        response = "Group %s inherits permissions from no groups and has no special permissions."
                module.send_message(response)
        
        if len(args) == 3:
            if args[0].lower() == "group":
                group_name = args[1]
                if args[2].lower() == "create":
                    if not self.permission_check(module, message.permissions_user, 'bp_permissions.groups.create'):
                        module.send_message("Sorry, you can't use this command!")
                        return
                    group = BP_Permissions.Group(module._permissions, group_name)
                    success = group.create()
                    if success:
                        response = "Created group %s" % group_name
                    else:
                        response = "Couldn't create group %s" % group_name
                    module.send_message(response)
                if args[2].lower() == "remove":
                    if not self.permission_check(module, message.permissions_user, 'bp_permissions.groups.remove'):
                        module.send_message("Sorry, you can't use this command!")
                        return
                    group = BP_Permissions.Group(module._permissions, group_name)
                    success = group.delete()
                    if success:
                        response = "Deleted group %s" % group_name
                    else:
                        response = "Couldn't delete group %s" % group_name
                    module.send_message(response)
        
        if len(args) == 4:
            if args[0].lower() == "user":
                userid = args[1]
                if args[2].lower() == "check":
                    permission = args[3]
                    if not self.permission_check(module, message.permissions_user, 'bp_permissions.check.others'):
                        module.send_message("Sorry, you can't use this command!")
                        return
                    user = BP_Permissions.User(module._permissions, userid)
                    display_name = module.get_display_name(userid)
                    has_perm = user.has_permission(permission)
                    if has_perm:
                        response = "User %s has the permission %s" % (display_name, permission)
                    else:
                        response = "User %s doesn't have the permission %s" % (display_name, permission)
                    module.send_message(response)
                if args[2].lower() == "add":
                    permission = args[3]
                    if not self.permission_check(module, message.permissions_user, 'bp_permissions.add'):
                        module.send_message("Sorry, you can't use this command!")
                        return
                    user = BP_Permissions.User(module._permissions, userid)
                    display_name = module.get_display_name(userid)
                    success = user.give_permission(permission)
                    if success:
                        response = "Added permission %s to user %s" % (permission, display_name)
                    else:
                        response = "Couldn't add permission %s to user %s" % (permission, display_name)
                    module.send_message(response)
                if args[2].lower() == "remove":
                    permission = args[3]
                    if not self.permission_check(module, message.permissions_user, 'bp_permissions.remove'):
                        module.send_message("Sorry, you can't use this command!")
                        return
                    user = BP_Permissions.User(module._permissions, userid)
                    display_name = module.get_display_name(userid)
                    success = user.revoke_permission(permission)
                    if success:
                        response = "Removed permission %s from user %s" % (permission, display_name)
                    else:
                        response = "Couldn't remove permission %s from user %s" % (permission, display_name)
                    module.send_message(response)
            if args[0].lower() == "group":
                group_name = args[1]
                if args[2].lower() == "check":
                    permission = args[3]
                    if not self.permission_check(module, message.permissions_user, 'bp_permissions.check.groups'):
                        module.send_message("Sorry, you can't use this command!")
                        return
                    group = BP_Permissions.Group(module._permissions, group_name).get()
                    if not group:
                        module.send_message("I couldn't find a group with that name.")
                        return
                    has_perm = group.has_permission(permission)
                    if has_perm:
                        response = "Group %s has the permission %s" % (group_name, permission)
                    else:
                        response = "Group %s doesn't have the permission %s" % (group_name, permission)
                    module.send_message(response)
                if args[2].lower() == "add":
                    permission = args[3]
                    if not self.permission_check(module, message.permissions_user, 'bp_permissions.groups.add'):
                        module.send_message("Sorry, you can't use this command!")
                        return
                    group = BP_Permissions.Group(module._permissions, group_name).get()
                    if not group:
                        module.send_message("I couldn't find a group with that name.")
                        return
                    success = group.give_permission(permission)
                    if success:
                        response = "Added permission %s to group %s" % (permission, group_name)
                    else:
                        response = "Couldn't add permission %s to group %s" % (permission, group_name)
                    module.send_message(response)
                if args[2].lower() == "remove":
                    permission = args[3]
                    if not self.permission_check(module, message.permissions_user, 'bp_permissions.groups.remove'):
                        module.send_message("Sorry, you can't use this command!")
                        return
                    group = BP_Permissions.Group(module._permissions, group_name).get()
                    if not group:
                        module.send_message("I couldn't find a group with that name.")
                        return
                    success = group.revoke_permission(permission)
                    if success:
                        response = "Removed permission %s from group %s" % (permission, group_name)
                    else:
                        response = "Couldn't remove permission %s from group %s" % (permission, group_name)
                    module.send_message(response)
                if args[2].lower() == "create":
                    parents = args[3]
                    if not self.permission_check(module, message.permissions_user, 'bp_permissions.groups.create'):
                        module.send_message("Sorry, you can't use this command!")
                        return
                    group = BP_Permissions.Group(module._permissions, group_name)
                    success = group.create()
                    if success:
                        group = group.get()
                        parents = parents.split(',')
                        for parent in parents:
                            group.add_inheritance(parent)
                        response = "Created group %s" % group_name
                    else:
                        response = "Couldn't create group %s" % group_name
                    module.send_message(response)
