from modules.commands import BP_CommandBase

class SongInfoCommand(BP_CommandBase.BaseCommand):

    def __init__(self, permissions, placeholders, cooldowns, aliases=[]):
        super(SongInfoCommand, self).__init__(permissions, placeholders, cooldowns, aliases=aliases)
        self.cmd_string = "!song"
        self.aliases = ['!current', '!currentsong', '!playing', '!lastsong', '!previous', '!prev', '!previoussong']
    
    def execute(self, message, module):
        last_song = module._player.last_song
        current_song = module._player.current_song

        if last_song:
            response = "The last song was %s - %s and the song that's playing at the moment is %s - %s" % (last_song['album'], last_song['title'], current_song['album'], current_song['title'])
        else:
            response = "The song that's playing at the moment is %s - %s" % (current_song['album'], current_song['title'])
        
        module.send_message(response)