from modules.commands.BP_CommandBase import BaseCommand

class AddCanCommand(BaseCommand):

    def __init__(self, permissions, placeholders, cooldowns, aliases=[]):
        super(AddCanCommand, self).__init__(permissions, placeholders, cooldowns, aliases)
        self.cmd_string = "!addcan"
        self.aliases = ['!addbear']
    
    def execute(self, message, module):
        cooldown = module.settings.addcan_cooldown
        if self.cooldowns.is_on_cooldown(self.cmd_string):
            return
        current = module._placeholders.increase_placeholder("cancount")
        response = module._placeholders.replace_placeholders(module.settings.addcan_response.replace('$cooldown', str(module.settings.addcan_cooldown)))
        module.send_message(response)
        self.cooldowns.set_cooldown(self.cmd_string, seconds=cooldown)