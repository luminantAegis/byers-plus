from modules.commands.BP_CommandBase import BaseCommand

class SearchCommand(BaseCommand):

    def __init__(self, permissions, placeholders, cooldowns, aliases=[]):
        super(SearchCommand, self).__init__(permissions, placeholders, cooldowns, aliases=aliases)
        self.cmd_string = "!search"
        self.aliases = ['!browse']
    
    def execute(self, message, module):
        match = module._player.request(message.get_parameter_string())

        if not match:
            response = "Sorry, I couldn't find anything that matched your query. Maybe try the advanced search? @%s" % message.username
        else:
            response = "I found %s - %s as best match for your query! @%s" % (match['album'], match['title'], message.username)
        
        module.send_message(response)

class AdvancedSearchCommand(BaseCommand):

    def __init__(self, permissions, placeholders, cooldowns, aliases=[]):
        super(AdvancedSearchCommand, self).__init__(permissions, placeholders, cooldowns, aliases=aliases)
        self.cmd_string = "!advancedsearch"
        self.aliases = ['!as']
    
    def execute(self, message, module):
        match = module._player.request(message.get_parameter_string(), advanced=True)

        if not match:
            response = "Sorry, I couldn't find anything that matched your query. @%s" % message.username
        else:
            response = "I found %s - %s as best match for your query! @%s" % (match['album'], match['title'], message.username)
        
        module.send_message(response)