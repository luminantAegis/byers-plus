from modules.commands.BP_CommandBase import BaseCommand

class HugCommand(BaseCommand):

    def __init__(self, permissions, placeholders, cooldowns, aliases=[]):
        super(HugCommand, self).__init__(permissions, placeholders, cooldowns, aliases)
        self.cmd_string = "!hug"
    
    def execute(self, message, module):
        current = module._placeholders.increase_placeholder("hugcount")
        
        if message.parameters:
            response = "Hug {0}: *hugs {1}*".format(str(current), message.parameters[0])
        else:
            response = "Hug {0}: *hugs @{1}*".format(str(current), message.username)
        module.send_message(response)