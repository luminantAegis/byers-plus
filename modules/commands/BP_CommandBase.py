# -*- coding: utf-8 -*-
class BaseCommand(object):

    def __init__(self, permissions, placeholders, cooldowns, aliases=[]):
        self.youtube = True
        self.twich = True
        self.discord = False
        self.mixer = True
        self.whispers = False
        self.cmd_string = ""
        self.aliases = aliases
        self.permissions = permissions
        self.placeholders = placeholders
        self.cooldowns = cooldowns
    
    def execute(self, message, module):
        pass