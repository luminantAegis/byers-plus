import os

from datetime import datetime, timedelta
from modules.commands.BP_CommandBase import BaseCommand
from modules.BP_Permissions import BP_Permissions

CONFIRM_REQUEST = {}

class SongRequestCommand(BaseCommand):

    def __init__(self, permissions, placeholders, cooldowns, aliases=[]):
        super(SongRequestCommand, self).__init__(permissions, placeholders, cooldowns, aliases)
        self.cmd_string = "!songrequest"
        self.aliases = ['!pidgeypls', '!requestsong', '!play', '!sr', '!sq', '!byerspls', '!addsong']
    
    def execute(self, message, module):
        perm_user = BP_Permissions.User(self.permissions, message.user)

        if module.get_hours(message.user) < module.settings.songrequest_hour_requirement:
            response = module.settings.songrequest_not_enough_hours_message.replace('$requestant', '@' + message.username)
            module.send_message(response)
            return

        if self.cooldowns.is_on_cooldown(self.cmd_string, userid=message.user) and not perm_user.has_permission('songrequest.skip_cooldown'):
            response = module.settings.songrequest_cooldown_message.replace('$cooldown_left', str(self.cooldowns.get_cooldown(self.cmd_string, userid=message.user, pretty_print=True))).replace('$requestant', '@' + message.username)
            module.send_message(response)
            return
        
        if message.get_parameter_string().strip() == "":
            module.send_message("You are missing the search query! %s" % '@' + message.username)
            return
        
        match = module._player.request(message.get_parameter_string())
        if not match:
            response = module.settings.songrequest_none_message.replace('$requestant', '@' + message.username)
            module.send_message(response)
            with open(os.path.join(module.main_dir, 'failed_requests.log'), 'a') as f:
                f.write("[%s] Failed request by %s, raw query (fuzzy): \"%s\"\n" % (str(datetime.now()), message.username, message.get_parameter_string()))
            return
        
        current = module._player.current_song
        pl_index = int(match['playlist_index'])-1
        if "%s.%d" % (current['playlist'], current['index']) == "%s.%d" % (match['playlist'], pl_index):
            self.cooldowns.set_cooldown('!!SONG_%s_%s' % (str(match['playlist']), str(match['playlist_index'])), seconds=module.settings.songrequest_song_cooldown_normal)
            module.send_message("You can't request this song, because it's playing at the moment. @%s" % message.username)
            return

        if self.cooldowns.is_on_cooldown('!!SONG_%s_%s' % (str(match['playlist']), str(match['playlist_index']))) and not perm_user.has_permission('songrequest.skip_song_cooldown'):
            cooldown = self.cooldowns.get_cooldown('!!SONG_%s_%s' % (str(match['playlist']), str(match['playlist_index'])), pretty_print=True)
            response = module.settings.songrequest_song_cooldown_message.replace('$requestant', '@' + message.username).replace('$cooldown_left', cooldown)
            for k, v in match.iteritems():
                response = response.replace('<' + k + '>', str(v))
            module.send_message(response)
            return

        response = module.settings.songrequest_confirm_message
        for k, v in match.iteritems():
            response = response.replace('<' + k + '>', str(v))
        response = response.replace('$requestant', '@' + message.username)
        global CONFIRM_REQUEST
        if module.settings.songrequest_yes_timeout != "-1":
            CONFIRM_REQUEST[message.user] = {'match': match, 'time': datetime.now() + timedelta(seconds=int(module.settings.songrequest_yes_timeout))}
        else:
            CONFIRM_REQUEST[message.user] = {'match': match, 'time': -1}
        module.send_message(response)


class AdvancedSongRequestCommand(BaseCommand):

    def __init__(self, permissions, placeholders, cooldowns, aliases=[]):
        super(AdvancedSongRequestCommand, self).__init__(permissions, placeholders, cooldowns, aliases)
        self.cmd_string = "!advancedrequest"
        self.aliases = ['!aq', '!ar', '!no']
    
    def execute(self, message, module):
        perm_user = BP_Permissions.User(self.permissions, message.user)

        if module.get_hours(message.user) < module.settings.songrequest_hour_requirement:
            response = module.settings.songrequest_not_enough_hours_message.replace('$requestant', '@' + message.username)
            module.send_message(response)
            return

        if self.cooldowns.is_on_cooldown("!songrequest", userid=message.user) and not perm_user.has_permission('songrequest.skip_cooldown'):
            response = module.settings.songrequest_cooldown_message.replace('$cooldown_left', str(self.cooldowns.get_cooldown("!songrequest", userid=message.user, pretty_print=True))).replace('$requestant', '@' + message.username)
            module.send_message(response)
            return
        
        if message.get_parameter_string().strip() == "":
            module.send_message('Try something like: !advancedrequest album:Vast+Error title:Zehanpuryu - full usage info coming soon!')
            return
        
        match = module._player.request(message.get_parameter_string(), advanced=True)
        if not match:
            response = module.settings.songrequest_none_message.replace('$requestant', '@' + message.username)
            module.send_message(response)
            with open(os.path.join(module.main_dir, 'failed_requests.log'), 'a') as f:
                f.write("[%s] Failed request by %s, raw query (advanced): \"%s\"\n" % (str(datetime.now()), message.username, message.get_parameter_string()))
            return

        current = module._player.current_song
        pl_index = int(match['playlist_index'])-1
        if "%s.%d" % (current['playlist'], current['index']) == "%s.%d" % (match['playlist'], pl_index):
            self.cooldowns.set_cooldown('!!SONG_%s_%s' % (str(match['playlist']), str(match['playlist_index'])), seconds=module.settings.songrequest_song_cooldown_normal)
            module.send_message("You can't request this song, because it's playing at the moment. @%s" % message.username)
            return

        if self.cooldowns.is_on_cooldown('!!SONG_%s_%s' % (str(match['playlist']), str(match['playlist_index']))) and not perm_user.has_permission('songrequest.skip_song_cooldown'):
            cooldown = self.cooldowns.get_cooldown('!!SONG_%s_%s' % (str(match['playlist']), str(match['playlist_index'])), pretty_print=True)
            response = module.settings.songrequest_song_cooldown_message.replace('$requestant', '@' + message.username).replace('$cooldown_left', cooldown)
            for k, v in match.iteritems():
                response = response.replace('<' + k + '>', str(v))
            module.send_message(response)
            return
            
        response = module.settings.songrequest_confirm_message
        for k, v in match.iteritems():
            response = response.replace('<' + k + '>', str(v))
        response = response.replace('$requestant', '@' + message.username)
        global CONFIRM_REQUEST
        if module.settings.songrequest_yes_timeout != "-1":
            CONFIRM_REQUEST[message.user] = {'match': match, 'time': datetime.now() + timedelta(seconds=int(module.settings.songrequest_yes_timeout))}
        else:
            CONFIRM_REQUEST[message.user] = {'match': match, 'time': -1}
        module.send_message(response)


class ConfirmSongRequestCommand(BaseCommand):

    def __init__(self, permissions, placeholders, cooldowns, aliases=[]):
        super(ConfirmSongRequestCommand, self).__init__(permissions, placeholders, cooldowns, aliases)
        self.cmd_string = "!yes"
    
    def execute(self, message, module):
        perm_user = BP_Permissions.User(self.permissions, message.user)

        global CONFIRM_REQUEST
        if not message.user in CONFIRM_REQUEST:
            response = module.settings.songrequest_yes_no_request_message.replace('$requestant', '@' + message.username)
            module.send_message(response)
            return

        if CONFIRM_REQUEST[message.user]['time'] != -1:
            if datetime.now() > CONFIRM_REQUEST[message.user]['time']:
                response = module.settings.songrequest_yes_timeout_message.replace('$requestant', '@' + message.username)
                module.send_message(response)
                del CONFIRM_REQUEST[message.user]
                return

        match = CONFIRM_REQUEST[message.user]['match']
        del CONFIRM_REQUEST[message.user]

        if self.cooldowns.is_on_cooldown('!!SONG_%s_%s' % (str(match['playlist']), str(match['playlist_index']))) and not perm_user.has_permission('songrequest.skip_song_cooldown'):
            cooldown = self.cooldowns.get_cooldown('!!SONG_%s_%s' % (str(match['playlist']), str(match['playlist_index'])), pretty_print=True)
            response = module.settings.songrequest_song_cooldown_message.replace('$requestant', '@' + message.username).replace('$cooldown_left', cooldown)
            for k, v in match.iteritems():
                response = response.replace('<' + k + '>', str(v))
            module.send_message(response)
            return

        cooldown = module.settings.songrequest_cooldown
        response = module.settings.songrequest_confirmed_message
        for k, v in match.iteritems():
            response = response.replace('<' + k + '>', str(v))
        response = response.replace('$requestant', '@' + message.username)
        response = response.replace('$time', str(datetime.now() + timedelta(seconds=int(cooldown))))
        seconds = module._player.play(match)
        song_cooldown = module.settings.songrequest_song_cooldown_normal
        if seconds >= 300:
            song_cooldown = module.settings.songrequest_song_cooldown_medium
        if seconds >= 600:
            song_cooldown = module.settings.songrequest_song_cooldown_long
        self.cooldowns.set_cooldown("!songrequest", seconds=int(cooldown), userid=message.user)
        self.cooldowns.set_cooldown('!!SONG_%s_%s' % (str(match['playlist']), str(match['playlist_index'])), seconds=song_cooldown)
        module.send_message(response)