"""
Cooldown management done right
"""

from datetime import datetime, timedelta
from modules.BP_Logging import BP_Logger

class BP_DaveStrider:

    def __init__(self, database):
        self.database = database
        self.logger = BP_Logger('turntechGodhead')
        self.database.execute("""
        CREATE TABLE IF NOT EXISTS bp_user_cooldowns (
            object TEXT,
            userid TEXT,
            last_cooldown INTEGER,
            next_use TEXT,
            comment TEXT,
            PRIMARY KEY(object, userid)
        );
        """)
        self.database.execute("""
        CREATE TABLE IF NOT EXISTS bp_global_cooldowns (
            object TEXT,
            last_cooldown INTEGER,
            next_use TEXT,
            comment TEXT,
            PRIMARY KEY(object)
        );
        """)
        self.database.commit()
    
    def set_cooldown(self, cd_object, userid=None, seconds=0, comment=None, global_seconds=None):
        next_use = datetime.now() + timedelta(seconds=seconds)
        if userid:
            self.logger.info("Setting cooldown of %s for user %s to %s (next use at %s)" % (cd_object, userid, self._pretty_print(seconds), next_use))
            query = "SELECT * FROM bp_user_cooldowns WHERE object = ? AND userid = ?;"
            query_result = self.database.execute(query, (cd_object, userid)).fetchone()
            if query_result is None:
                query = "INSERT INTO bp_user_cooldowns VALUES (?, ?, ?, ?, ?);"
                self.database.execute(query, (cd_object, userid, seconds, str(next_use), comment))
                self.database.commit()
                return seconds
            query = "UPDATE bp_user_cooldowns SET last_cooldown = ?, next_use = ?, comment = ? WHERE object = ? AND userid = ?;"
            self.database.execute(query, (seconds, str(next_use), comment, cd_object, userid))
            self.database.commit()
            if global_seconds:
                self.set_cooldown(cd_object, seconds=global_seconds, comment=comment)
            return seconds
        else:
            self.logger.info("Setting cooldown of %s to %s (next use at %s)" % (cd_object, self._pretty_print(seconds), next_use))
            query = "SELECT * FROM bp_global_cooldowns WHERE object = ?;"
            query_result = self.database.execute(query, (cd_object,)).fetchone()
            if query_result is None:
                query = "INSERT INTO bp_global_cooldowns VALUES (?, ?, ?, ?);"
                self.database.execute(query, (cd_object, seconds, str(next_use), comment))
                self.database.commit()
                return seconds
            query = "UPDATE bp_global_cooldowns SET last_cooldown = ?, next_use = ?, comment = ? WHERE object = ?;"
            self.database.execute(query, (seconds, str(next_use), comment, cd_object))
            self.database.commit()
            return seconds
    
    def _pretty_print(self, seconds):
        minutes, seconds = divmod(seconds, 60)
        hours, minutes = divmod(minutes, 60)

        periods = [('hours', hours), ('minutes', minutes), ('seconds', seconds)]
        time_string = ', '.join('{} {}'.format(value, name)
                                for name, value in periods
                                if value)
        return time_string

    def get_cooldown(self, cd_object, userid=None, pretty_print=False):
        result_seconds = 0
        if userid:
            query = "SELECT next_use FROM bp_user_cooldowns WHERE object = ? AND userid = ?;"
            query_result = self.database.execute(query, (cd_object, userid)).fetchone()
            if query_result is None:
                result_seconds = 0
            else:
                date_val = datetime.strptime(str(query_result[0]), '%Y-%m-%d %H:%M:%S.%f')
                date_diff = date_val - datetime.now()
                result_seconds = int(date_diff.total_seconds())
            global_cd = self.get_cooldown(cd_object)
            if global_cd > result_seconds:
                result_seconds = global_cd
            if result_seconds <= 0:
                result_seconds = 0
        else:
            query = "SELECT next_use FROM bp_global_cooldowns WHERE object = ?;"
            query_result = self.database.execute(query, (cd_object,)).fetchone()
            if query_result is None:
                result_seconds = 0
            else:
                date_val = datetime.strptime(str(query_result[0]), '%Y-%m-%d %H:%M:%S.%f')
                date_diff = date_val - datetime.now()
                seconds_left = int(date_diff.total_seconds())
                if seconds_left <= 0:
                    result_seconds = 0
                else:
                    result_seconds = seconds_left
        if pretty_print:
            return self._pretty_print(result_seconds)
        else:
            return result_seconds

    def increase_cooldown(self, cd_object, userid=None, seconds=0, global_seconds=None):
        current_cooldown = self.get_cooldown(cd_object, userid)
        additional = current_cooldown + seconds
        additional_global = None
        if global_seconds:
            additional_global = current_cooldown + global_seconds
        return self.set_cooldown(cd_object, userid, additional, global_seconds=additional_global)

    def decrease_cooldown(self, cd_object, userid=None, seconds=0, global_seconds=None):
        current_cooldown = self.get_cooldown(cd_object, userid)
        additional = current_cooldown - seconds
        if additional <= 0:
            additional = 0
        additional_global = None
        if global_seconds:
            additional_global = current_cooldown - global_seconds
            if additional_global <= 0:
                additional_global = 0
        return self.set_cooldown(cd_object, userid, additional, global_seconds=additional_global)

    def remove_cooldown(self, cd_object, userid=None, global_seconds=None):
        return self.set_cooldown(cd_object, userid, 0, "Removed cooldown", global_seconds)

    def is_on_cooldown(self, cd_object, userid=None):
        cd = self.get_cooldown(cd_object, userid)
        self.logger.debug("Is on cooldown: %d" % cd)
        return cd > 0