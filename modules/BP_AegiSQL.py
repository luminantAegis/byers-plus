"""
Handles database access
"""
import os
if 'DEBUG' in os.environ:
    from modules.env import clr
else:
    import clr
clr.AddReference("IronPython.SQLite.dll")
clr.AddReference("IronPython.Modules.dll")
import _sqlite3 as sqlite3
from modules.BP_Logging import BP_Logger

class AegiSQL:

    def __init__(self, db_name):
        self._database = db_name
        self._connection = sqlite3.connect(db_name, check_same_thread=False)
        self._cursor = self._connection.cursor()
        self.logger = BP_Logger('AegiSQL')
    
    def close(self):
        if self._connection is None:
            return
        self._connection.close()
    
    def execute(self, sql, params=()):
        self.logger.debug("Executing SQL statement %s..." % sql)
        if params:
            self._cursor.execute(sql, params)
        else:
            self._cursor.execute(sql)
        return self._cursor
    
    def commit(self):
        self._connection.commit()
    
    def __del__(self):
        self.close()
