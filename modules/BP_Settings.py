import io
import json
class ByersSettings:

    def __init__(self, settings):
        self.file = settings
        self.solr_port = None
        self.beefweb_port = None
        self.addcan_cooldown = None
        self.addcan_response = None
        self.songrequest_cooldown = None
        self.songrequest_cooldown_message = None
        self.songrequest_confirm_message = None
        self.songrequest_none_message = None
        self.songrequest_confirmed_message = None
        try:
            with io.open(settings, "r", encoding='utf-8-sig') as f:
                self.reload(f.read())
        except:
            print("Couldn't read settings, using defaults...")
            to_be_serialized = {}
            config = None
            with io.open('UI_Config.json', 'r', encoding='utf-8') as f:
                config = dict(json.load(f))
            for k, v in config.iteritems():
                if k == 'output_file':
                    continue
                setting = k
                value = v['value']
                to_be_serialized[setting] = value
            self.reload(json.dumps(to_be_serialized))

    
    @property
    def has_valid_database(self):
        return self.pg_host and self.pg_host != "" and self.pg_port and self.pg_user and self.pg_user != "" and self.pg_password and self.pg_password != "" and self.pg_database and self.pg_database != ""
    
    def reload(self, json_str):
        self.__dict__.update(json.loads(json_str, encoding="utf-8-sig"))
    
    def save(self):
        with io.open(self.file, "w", encoding='utf-8-sig') as f:
            json.dump(self, f, encoding="utf-8-sig")