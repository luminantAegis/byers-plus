"""Handles commands of Byers+"""
import importlib
import os
import inspect
from modules.commands.BP_CommandBase import BaseCommand
from modules.BP_Utils import StringView
from modules.BP_Permissions import BP_Permissions
from modules.BP_Logging import BP_Logger

def get_children_in_module(mod, base):
    classlist = []
    for clsname, clas in inspect.getmembers(mod, inspect.isclass):
        if base in inspect.getmro(clas):
            classlist.append((clsname, clas))
    return classlist

class ChatMessage:
    """Represents a StreamLabs chat message"""

    def __init__(self, data_obj, module):
        self.user = data_obj.User
        self.username = data_obj.UserName
        self.permissions_user = BP_Permissions.User(module._permissions, self.user)
        self.message = data_obj.Message
        self.raw_data = data_obj.RawData
        self.is_chat_message = data_obj.IsChatMessage()
        self.is_raw = data_obj.IsRawData()
        self.from_twitch = data_obj.IsFromTwitch()
        self.from_youtube = data_obj.IsFromYoutube()
        self.from_mixer = data_obj.IsFromMixer()
        self.from_discord = data_obj.IsFromDiscord()
        self.is_whisper = data_obj.IsWhisper()
        self.parameters = []
        view = StringView(self.message)
        while not view.eof:
            self.parameters.append(view.get_quoted_word())
            view.skip_ws()
        self.command_str = self.parameters[0]
        self.parameters = self.parameters[1:]

    def __str__(self):
        return self.message
    
    def get_parameter_string(self):
        return self.message[len(self.command_str)+1:]


class CommandLoader:

    def __init__(self, module):
        self.module = module
        self.logger = BP_Logger('CommandLoader')
        self.reload_commands()
    
    def reload_commands(self):
        self._commands = {}
        directory = os.path.join(os.path.dirname(__file__), "commands")
        for file in os.listdir(directory):
            if not file.endswith(".py") or file == "__init__.py":
                continue
            modulename = "modules.commands.%s" % file[:len(file)-3]
            module = importlib.import_module(modulename)
            clslist = get_children_in_module(module, BaseCommand)
            for classname, clas in clslist:
                if classname == "BaseCommand":
                    continue
                self.logger.info("Adding command %s to command list" % classname)
                cmd = clas(self.module._permissions, self.module._placeholders, self.module._cooldowns)
                self._commands[cmd.cmd_string] = cmd
                for alias in cmd.aliases:
                    self._commands[alias] = cmd
    
    def handle_commands(self, data):
        chat_message = ChatMessage(data, self.module)
        if chat_message.command_str in self._commands:
            cmd = self._commands[chat_message.command_str]
            self.logger.info("Executing command %s with class %s..." % (chat_message.message, repr(cmd)))
            cmd.execute(message=chat_message, module=self.module)
            